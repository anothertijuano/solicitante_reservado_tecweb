import pymongo
import datetime
from Lab import Lab
import pytz

DB=pymongo.MongoClient("mongodb+srv://root:admin@cluster0-vtykr.mongodb.net/test?retryWrites=true&w=majority")

reservado=DB["RESERVADO"]
horarios=reservado["HORARIOS"]
materias=reservado["MATERIAS"]

dias={
      "1" : "Lunes", 
      "2" : "Martes",
      "3" : "Miercoles",
      "4" : "Jueves", 
      "5" : "Viernes",
      "6" : "Viernes",
      "7" : "Viernes"
      }

horas={
         1: range(70000,82959),
         2: range(83000,95959),
         3: range(103000,115959),
         4: range(120000,132959),
         5: range(133000,145959),
         6: range(150000,162959),
         7: range(163000,180000),
         8: range(183000,195959),
         9: range(200000,213000)
         }

def getLista(diccionario, dato):
   lista=list()
   find=diccionario.find()
   print(find)
   for elemento in find:
      lista.append(elemento[dato])
   print(lista)
   return(lista)

def getMaterias():
   print("Materias: ")
   return(getLista(materias, "nombre"))

def getLabs():
   print("Horarios: ")
   horario=getHorario()
   lista=list()
   for i in horarios.find():
      if(i[horario]=="Recuperación"):
         tmp=Lab()
         tmp.nombre=i['nombre']
         tmp.numero=i['salon']
         lista.append(tmp)
   print(lista)
   return(lista)

def getHorario():
   fechahora = datetime.datetime.now()
   numeroDia=fechahora.strftime("%w")
   Hora=int(fechahora.strftime("%H"))
   Min=int(fechahora.strftime("%M"))
   Seg=int(fechahora.strftime("%S"))
   tiempo=(Hora*10000)+(Min*100)+(Seg)
   print("tiempo: "+str(tiempo))   
   hora="8"
   for i in range(1,len(horas)+1):
      if tiempo in horas[i]:
         hora=str(i)
   return(dias[numeroDia]+hora)
         
def getFecha():
   fecha=datetime.datetime.now(pytz.timezone('America/Mexico_City'))
   numeroDia=fecha.strftime("%w")
   diaMes=fecha.strftime("%d")
   return(dias[numeroDia]+" "+diaMes)


