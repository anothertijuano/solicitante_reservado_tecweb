function habilitarSubmit(forma)
{
   var solicitar=document.getElementById('btnSolicitar');
   var nombre=document.getElementById('nombreEstudiante').value;
   var boleta=document.getElementById('boletaEstudiante').value;
   var materia=document.getElementById('materiaEstudiante').value;
   if(nombre.length  > 0 &&
      boleta.length  > 0)
   {
      solicitar.disabled = false;   
   }

}

function validar(forma)
{
   var solicitar=document.getElementById('btnSolicitar');
   var nombre=document.getElementById('nombreEstudiante');
   var boleta=document.getElementById('boletaEstudiante');
   var materia=document.getElementById('materiaEstudiante');
   if(nombre.value.length  < 10)
   {
      alert("Introduzca nombre completo");
      nombre.focus();
      nombre.select();
      return false;
   }
   if(boleta.value.length  != 10 || isNaN(boleta.value))
   {
      alert("Verifica el número de boleta");
      boleta.focus();
      boleta.focus();
      return false;
   }
}
