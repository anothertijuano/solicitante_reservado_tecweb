# views.py
import sys
from flask import render_template
from flask import request
from conector import *
from app import app
from Lab import Lab

@app.route('/')
def index():
   #return('Hello World')
   fecha=getFecha()
   tmp1=Lab()
   tmp2=Lab()
   tmp3=Lab()
   tmp4=Lab()
   tmp1.nombre="Sistemas I"
   tmp1.numero="201"
   tmp2.nombre="Sistemas II"
   tmp2.numero="202"
   tmp3.nombre="Redes I"
   tmp3.numero="101"
   tmp4.nombre="Redes II"
   tmp4.numero="102"
   labDisponibles=getLabs()
   #labDisponibles=[tmp1,tmp2,tmp3,tmp4]
   if( not labDisponibles ):
      return render_template("indexNoLab.html", fecha=fecha)
   return render_template("index.html", fecha=fecha, labDisponibles=labDisponibles)

@app.route('/vistaLab')
def vistaLab():
   lab=Lab()
   lab.nombre=request.args.get('labNombre')
   lab.numero=request.args.get('labNumero')
   #materias=["Cálculo","Algoritmia","Física","Instrumentación"]
   materias=getMaterias()
   return render_template("vistaLab.html", lab=lab, materias=materias)

@app.route('/confirmacion')
def confirmacion():
   return render_template("confirmacion.html")

#@app.route('/success', methods = ['GET', 'POST'])  
#def success():  
   #return render_template("report.html", coops=coops, project=project) 
   #return render_template("uploadError.html") 
