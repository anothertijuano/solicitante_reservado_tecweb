# Solicitante
### Interfaz web móvil para realizar la solicitud de préstamo de equipo de cómputo en los laboratorios de la ESCOM

**Reservado** es un sistema que permite apoyar en el proceso de solicitud de equipo de cómputo en los laboratorios. Está diseñado para apoyar más no reemplazar el actual sistema de préstamos.

Esta interfaz, pensada para ser ejecutada en el dispositivo móvil de él o la estudiante;

1. Permite visualizar los laboratorios disponibles.
2. Permite visualizar la ubicación del laboratorios.
3. Permite capturar los datos necesarios para solicitar el uso del laboratorio.

Los datos que se capturan son los siguientes:
- Nombre Completo
- Número de boleta
- Materia para la que se requiere el laboratorio

Durante el desarrollo de este sistema se generó una base de datos con MongoDB para almacenar los nombres y horarios de los laboratorios, así como una lista de las materias disponibles. (Estos datos son ilustrativos dado que en el momento del desarrollo de este trabajo el acceso a la escuela no es posible y la información no puede ser verificada).

![Visualización gráfica de MongoDB](https://i.imgur.com/8K0432G.png "MongoDB Screenshot")

---
Al inicio se muestra a la o él usuario la lista de laboratorios disponibles a la hora de la consulta. Se agregó un laboratorio ficticio siempre disponible con fines de visualización del sistema.

Una vez seleccionado se le solicita a el o la estudiante que introduzca los datos necesarios para realizar la solicitud. El sistema hace una validación de la estructura de los mismos con JavaScript para evitar errores en la entrada de datos.

Finalmente se realiza la petición que se almacena en la base de datos, análogo a como actualmente esta petición se registra en los cuadernos presentes en los laboratorios, cabe mencionar que las peticiones no se aprueban automáticamente, esto sigue estando bajo el control de la persona encargada de laboratorio, quíen podrá aceptar o negar la petición.

![Flujo de petición de laboratorio](https://i.imgur.com/YfyrNt7.mp4)

---
#### Para el desarrollo de este trabajo se llevaron a cabo los siguientes puntos.

- Las plantillas se crean con HTML y se estilizaron con CSS.
- Las vistas del sistema se generan de manera dinámica haciendo uso de python con Flask y Jinja2.
- Los datos del formulario se validan con JavaScript.
- La base de datos no relacional se implementó con MongoDB.
- Para facilitar su presentación, el sistema se montó en Heroku.

---
#### Se deben tomar las siguientes modificaciones que se hicieron al sistema intecnionalmente con fines demostrativos:
- La fecha en Sabados y Domingos aparecerá como Viernes
- Se creó un laboratorio ficticio siempre disponible
- Los horararios y números de salones no son meramente ilustrativos.
